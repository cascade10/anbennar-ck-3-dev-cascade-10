﻿farrani = {
	trigger = {
		OR = {
			AND = {
				scope:culture = culture:adeanic
				scope:other_culture = { has_cultural_pillar = heritage_damesheader }
			}
			AND = {
				scope:other_culture = culture:adeanic
				scope:culture = { has_cultural_pillar = heritage_damesheader }
			}
		}
	}
	hybrid = yes
}

calindalic = {
	trigger = {
		matches_cultures = {
			culture_1 = culture:moon_elvish
			culture_2 = culture:marcher
		}
	}
	hybrid = yes
}

urionmari = {
	trigger = {
		matches_cultures = {
			culture_1 = culture:moon_elvish
			culture_2 = culture:castellyrian
		}
	}
	hybrid = yes
}

nuralenic = {
	trigger = {
		matches_cultures = {
			culture_1 = culture:moon_elvish
			culture_2 = culture:gawedi
		}
	}
	hybrid = yes
}

thilosian = {
	trigger = {
		matches_cultures = {
			culture_1 = culture:moon_elvish
			culture_2 = culture:wexonard
		}
	}
	hybrid = yes
}

black_reachman = {
	trigger = {
		OR = {
			AND = {
				scope:culture = { has_cultural_pillar = language_reachman_common } 
				scope:other_culture = { has_cultural_pillar = heritage_gerudian }
			}
			AND = {
				scope:other_culture = { has_cultural_pillar = language_reachman_common }
				scope:culture = { has_cultural_pillar = heritage_gerudian }
			}
		}
	}
	hybrid = yes
}
