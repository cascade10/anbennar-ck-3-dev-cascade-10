﻿name_list_kheteratan = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {
		Aahmes Aakh Ahwahmet Akil Akir Amer Ankhaf Ankhesen Ankhetef Arashk Babar Batobty Bayek Bhenak Bircenotep Bonep Bupalos Chufar Crovacef Dajasht Djede Duaenre Duzar Entef 
		Erbay Gesaaru Hapose Hekeb Hori Hyaclos Irsmaset Jabar Jabirgar Janebsir Kair Karom Kawab Kenmu Khaem Khaeroc Khair Khorec Khotep Korydon Lykidas Madalac Maracout Mekhaf Menes 
		Menkare Menkaunswt Menkhe Mentuher Merer Murdas Nahab Nazem Nefkar Nikaure Nuhem Otatrac Panehesy Panthes Pasebakh Qareh Raad Rabon Rawer Rernebty Res Sab Saban Sedoric Seker 
		Sekhem Semut Seneferu Senemet Senwosret Sepeshet Seti Shep Shishak Shoshenq Siout Tariush Tasherit Teganret Tumain Tydeos Waset Yawhotep
	}
	female_names = {
		Ahti Ahwseenu Amina Anqa Antarta Arna Bakrana Bircenare Chena Crova Defra Esi Esia Falaha Feruaten Henuttaui Hetepheres Janebisa Jenda Kemsaf Keta Khafra Khamer Khenta
		Khuenre Leta Maatkare Madsa Merit Nadia Naunet Neferu Nesubane Panya Raziya Rekhetre Resa Saba Sabana Sedora Sekhem Sira Subira Taia Taure Tentamun Thema Tiye Toriya Tuma Zalika
	}

	dynasty_of_location_prefix = "dynnp_szel"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}

name_list_akasi = {

	cadet_dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}

	dynasty_names = {
		{ "dynnp_de" "dynn_Bohun" }
		{ "dynnp_de" "dynn_Vere" }
		{ "dynnp_de" "dynn_Gand" }
		"dynn_Myall"
		{ "dynnp_de" "dynn_Turberville" }
		{ "dynnp_de" "dynn_Holland" }
		"dynn_Butler"
		{ "dynnp_of" "dynn_Thornham" }
		{ "dynnp_de" "dynn_Camville" }
		"dynn_Breakspear"
		{ "dynnp_of" "dynn_Itchington" }
		{ "dynnp_of" "dynn_Northall" }
		{ "dynnp_de" "dynn_Montagu" }
		{ "dynnp_de" "dynn_Umfraville" }
		"dynn_FitzPeter"
		"dynn_Belles-mains"
		{ "dynnp_of" "dynn_Warwick" }
		"dynn_FitzAlan"
		{ "dynnp_de" "dynn_Beauchamp" }
		{ "dynnp_of" "dynn_Ilchester" }
		{ "dynnp_de" "dynn_Raleigh" }
		{ "dynnp_de" "dynn_Stratford" }
		"dynn_Becket"
		{ "dynnp_de" "dynn_la_Pole" }
	}
	
	
	male_names = {
		Adlire Akhil Amsal Arsheg Asay Bashal Benakh Bihren Isil Iushen Khanar Mehian Melen Nahil Rekhi Rimhe Sayar Sharil Shedorikh
	}
	female_names = {
		Adliak Amsala Arshag Ayahal Ayar Bhiakh Bhiaseg Isila Kharia Melan Rekha Rimha Sayir Shala Silna Simal Takhal Tarya Tise
	}

	dynasty_of_location_prefix = "dynnp_szel"

	# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		{ name = "mercenary_company_white_company" coat_of_arms = "mc_white_company" }
		{ name = "mercenary_company_hawkwoods_band" }
	}
}