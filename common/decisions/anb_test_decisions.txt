﻿#These should get removed after a point, also localized as they spam the log
try_to_wield_calindal_test_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = try_to_wield_calindal_test_decision_desc
	selection_tooltip = try_to_wield_calindal_test_decision_tooltip

	is_shown = {
		debug_only = yes
	}
	effect = {
		trigger_event = blademarches_succession.0001
		remove_character_modifier = blinded_by_calindal
		remove_character_modifier = wielded_calindal
		remove_trait = blind
		custom_tooltip = blademarches_success_modifiers_tooltip
	}

	ai_check_interval = 0
}

wield_calindal_test_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = wield_calindal_test_decision_desc
	selection_tooltip = wield_calindal_test_decision_tooltip

	is_shown = {
		debug_only = yes
	}
	effect = {
		trigger_event = blademarches_succession.0001
		remove_character_modifier = blinded_by_calindal
		add_character_modifier = wielded_calindal
	}

	ai_check_interval = 0
}

get_blinded_by_calindal_test_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = get_blinded_by_calindal_test_decision_desc
	selection_tooltip = get_blinded_by_calindal_test_decision_tooltip

	is_shown = {
		debug_only = yes
	}
	effect = {
		trigger_event = blademarches_succession.0001
		add_character_modifier = blinded_by_calindal
		remove_character_modifier = wielded_calindal
	}

	ai_check_interval = 0
}

convert_to_regent_court_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = convert_to_regent_court_decision_desc
	selection_tooltip = convert_to_regent_court_decision_tooltip

	is_shown = {
		debug_only = yes
	}
	effect = {
		found_faith_no_conversion = {
			FAITH = faith:regent_court
		}
		set_character_faith = faith:regent_court
	}

	ai_check_interval = 0
}

learn_a_bilingual_language_descision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = learn_a_bilingual_language_descision_desc
	selection_tooltip = learn_a_bilingual_language_descision_tooltip	

	is_shown = {
		debug_only = yes
		can_learn_language_from_culture = yes
	}

	effect = {
		save_scope_as = character

		# Saves the language to learn as culture_to_learn_from
		culture = {
			save_language_to_learn_from_culture = yes
		}

		add_prestige = miniscule_prestige_gain
		learn_language_of_culture = scope:culture_to_learn_from
	}

	ai_check_interval = 0
}