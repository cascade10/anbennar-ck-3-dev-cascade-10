﻿
give_corset_burial_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_personal_religious.dds"
	}

	desc = give_corset_burial_decision_desc
	selection_tooltip = give_corset_burial_decision_tooltip

	is_shown = {
		faith = {
			has_doctrine_parameter = corset_burials_active
		}
		has_variable = ancestor_to_bury
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
	}

	effect = {
		show_as_tooltip = {
			add_piety = major_piety_value
			if = {
				limit = {
					any_vassal = {
						faith = {
							has_doctrine_parameter = corset_burials_active
						}
					}
				}
				every_vassal = {
					limit = {
						faith = {
							has_doctrine_parameter = corset_burials_active
						}
					}
					custom = give_corset_burial_vassals
					add_opinion = {
						modifier = pleased_opinion
						target = root
						opinion = 20
					}
				}
			}
		}
		trigger_event = anb_religious_decision.0001
	}

	ai_check_interval = 36
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

recite_skaldic_tale_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_personal_religious.dds"
	}
	desc = recite_skaldic_tale_decision_desc
	selection_tooltip = recite_skaldic_tale_decision_tooltip
	
	is_shown = {
		faith = {
			has_doctrine_parameter = skaldic_tales_active
		}
		has_skaldic_tale_active = no
		NOT = { has_variable = reciting_skaldic_tales }
	}
	
	is_valid = {
	}
	is_valid_showing_failures_only = {
	}
	
	cost = {
		piety = 100
	}
	
	effect = {
		show_as_tooltip = {
			if = {
				limit = {
					any_vassal = {
						faith = {
							has_doctrine_parameter = skaldic_tales_active
						}
					}
				}
				every_vassal = {
					limit = {
						faith = {
							has_doctrine_parameter = skaldic_tales_active
						}
					}
					custom = recite_skaldic_tale_vassals
					add_opinion = {
						modifier = pleased_opinion
						target = root
						opinion = 10
					}
				}
			}
		}
		
		hidden_effect = {
			set_variable = reciting_skaldic_tales
		}
		
		trigger_event = anb_religious_decision.0501
	}
	ai_check_interval = 36
	
	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 100
	}
}

select_personal_deity_eidoueni_decision = { # Anbennar
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = select_personal_deity_eidoueni_decision_desc
	selection_tooltip = select_personal_deity_germanic_decision_tooltip

	is_shown = {
		religion = religion:lencori_religion
		faith = { has_doctrine_parameter = select_personal_god_active }
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
		# Have to at _least_ not be in piety debt.
		piety >= 0
	}

	cooldown = { years = 2 }

	effect = {
		# Show the possible options.
		show_as_tooltip = {
			random_list = {
				desc = select_personal_deity_eidoueni_decision_tt
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_artanos
					add_character_modifier = artanos_deity
				}
				100 = {
					trigger = { is_vaguely_danish_bhakti_trigger = no }
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_merisse
					add_character_modifier = merisse_deity
				}
				100 = {
					trigger = { is_vaguely_danish_bhakti_trigger = yes }
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_trovecos
					add_character_modifier = trovecos_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_careslobos
					add_character_modifier = careslobos_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_asmirethin
					add_character_modifier = asmirethin_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_damarta
					add_character_modifier = damarta_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_belouina
					add_character_modifier = belouina_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_dercanos
					add_character_modifier = dercanos_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_sorbodua
					add_character_modifier = sorbodua_deity

				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eidoueni_decision.select_turanos
					add_character_modifier = turanos_deity
				}
			}
		}
		# And the actual effect.
		trigger_event = anb_religious_decision.0104
	}

	ai_check_interval = 96
	
	ai_potential = {
		piety >= minor_piety_value
		NOR = {
			has_character_modifier = artanos_deity
			has_character_modifier = turanos_deity
			has_character_modifier = sorbodua_deity
			has_character_modifier = dercanos_deity
			has_character_modifier = belouina_deity
			has_character_modifier = asmirethin_deity
			has_character_modifier = careslobos_deity
			has_character_modifier = trovecos_deity
			has_character_modifier = merisse_deity
		}
	}

	ai_will_do = {
		base = 100
	}
}

select_personal_deity_eseral_mitellu_decision = { # Anbennar
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = select_personal_deity_eseral_mitellu_decision_desc
	selection_tooltip = select_personal_deity_germanic_decision_tooltip

	is_shown = {
		faith = faith:cult_of_the_eseral_mitellu
		faith = { has_doctrine_parameter = select_personal_god_active }
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
		# Have to at _least_ not be in piety debt.
		piety >= 0
	}

	cooldown = { years = 2 }

	effect = {
		# Show the possible options.
		show_as_tooltip = {
			random_list = {
				desc = select_personal_deity_eseral_mitellu_decision_tt
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_edurunigal
					add_character_modifier = edurunigal_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_nimensar
					add_character_modifier = nimensar_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_ninu
					add_character_modifier = ninu_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_suhus
					add_character_modifier = suhus_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_zamagur
					add_character_modifier = zamagur_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_dumegir
					add_character_modifier = dumegir_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_gelsibir
					add_character_modifier = gelsibir_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_tambora
					add_character_modifier = tambora_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_kirasi
					add_character_modifier = kirasi_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_gikud
					add_character_modifier = gikud_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_genna
					add_character_modifier = genna_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_pinnagar
					add_character_modifier = pinnagar_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_eseral_mitellu_decision.select_sidim
					add_character_modifier = sidim_deity
				}
			}
		}
		# And the actual effect.
		trigger_event = anb_religious_decision.0301
	}

	ai_check_interval = 96
	
	ai_potential = {
		piety >= minor_piety_value
		NOR = {
			has_character_modifier = edurunigal_deity
			has_character_modifier = nimensar_deity
			has_character_modifier = ninu_deity
			has_character_modifier = suhus_deity
			has_character_modifier = zamagur_deity
			has_character_modifier = dumegir_deity
			has_character_modifier = gelsibir_deity
			has_character_modifier = tambora_deity
			has_character_modifier = kirasi_deity
			has_character_modifier = gikud_deity
			has_character_modifier = genna_deity
			has_character_modifier = pinnagar_deity
			has_character_modifier = sidim_deity
		}
	}

	ai_will_do = {
		base = 100
	}
}

select_personal_deity_erbatrasi_decision = { # Anbennar
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = select_personal_deity_erbatrasi_decision_desc
	selection_tooltip = select_personal_deity_germanic_decision_tooltip

	is_shown = {
		OR = {
			faith = faith:way_of_the_sea
			faith = faith:cult_of_the_wise_master
		}
		faith = { has_doctrine_parameter = select_personal_god_active }
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
		# Have to at _least_ not be in piety debt.
		piety >= 0
	}

	cooldown = { years = 2 }

	effect = {
		# Show the possible options.
		show_as_tooltip = {
			random_list = {
				desc = select_personal_deity_erbatrasi_decision_tt
				100 = {
					show_chance = no
					desc = select_personal_deity_erbatrasi_decision.select_sarnagir
					add_character_modifier = sarnagir_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_erbatrasi_decision.select_lahmas
					add_character_modifier = lahmas_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_erbatrasi_decision.select_amasuri
					add_character_modifier = amasuri_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_erbatrasi_decision.select_brasan
					add_character_modifier = brasan_deity
				}
			}
		}
		# And the actual effect.
		trigger_event = anb_religious_decision.0304
	}

	ai_check_interval = 96
	
	ai_potential = {
		piety >= minor_piety_value
		NOR = {
			has_character_modifier = sarnagir_deity
			has_character_modifier = lahmas_deity
			has_character_modifier = amasuri_deity
			has_character_modifier = brasan_deity
		}
	}

	ai_will_do = {
		base = 100
	}
}

select_personal_deity_sundancer_paragons_decision = { # Anbennar
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}

	desc = select_personal_deity_sundancer_paragons_decision_desc
	selection_tooltip = select_personal_deity_germanic_decision_tooltip

	is_shown = {
		faith = faith:sundancer_paragons
		faith = { has_doctrine_parameter = select_personal_god_active }
	}

	is_valid = {
	}

	is_valid_showing_failures_only = {
		# Have to at _least_ not be in piety debt.
		piety >= 0
	}

	cooldown = { years = 2 }

	effect = {
		# Show the possible options.
		show_as_tooltip = {
			random_list = {
				desc = select_personal_deity_sundancer_paragons_decision_tt
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_allania
					add_character_modifier = allania_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_elizarian
					add_character_modifier = elizarian_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_jaherion
					add_character_modifier = jaherion_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_jaerelian
					add_character_modifier = jaerelian_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_aldarien
					add_character_modifier = aldarien_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_erelessa
					add_character_modifier = erelessa_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_velara
					add_character_modifier = velara_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_lezlindel
					add_character_modifier = lezlindel_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_andrellion
					add_character_modifier = andrellion_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_jaddarion
					add_character_modifier = jaddarion_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_taelarian
					add_character_modifier = taelarian_deity
				}
				100 = {
					show_chance = no
					desc = select_personal_deity_sundancer_paragons_decision.select_daraztarion
					add_character_modifier = daraztarion_deity
				}
			}
		}
		# And the actual effect.
		trigger_event = anb_religious_decision.0305
	}

	ai_check_interval = 96
	
	ai_potential = {
		piety >= minor_piety_value
		NOR = {
			has_character_modifier = allania_deity
			has_character_modifier = elizarian_deity
			has_character_modifier = jaherion_deity
			has_character_modifier = jaerelian_deity
			has_character_modifier = aldarien_deity
			has_character_modifier = erelessa_deity
			has_character_modifier = velara_deity
			has_character_modifier = lezlindel_deity
			has_character_modifier = andrellion_deity
			has_character_modifier = jaddarion_deity
			has_character_modifier = taelarian_deity
			has_character_modifier = daraztarion_deity
		}
	}

	ai_will_do = {
		base = 100
	}
}

renovate_bulwari_temple_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}
	decision_group_type = major
	desc = renovate_bulwari_temple_decision_desc
	selection_tooltip = renovate_bulwari_temple_decision_tooltip
	
	is_shown = {
		religion_group = rf_bulwari
		has_title = c_bulwar
		NOT = {
			title:b_eduz_rakadags.title_province = {
				OR = {
					has_province_modifier = renovating_bulwar_temple
					has_building = temple_bulwar_03
				}
				
			}
		}
	}

	is_valid = {
		has_title = title:e_bulwar
		piety_level >= high_piety_level
		prestige_level >=  high_prestige_level
	}

	cooldown = { years = 2 }

	effect = {
		trigger_event = anb_minor_decisions.0004
	}

	cost = {
		gold = 1200
		piety = 300
	}
}	

end_renovation_bulwari_temple_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_major_religion.dds"
	}
	decision_group_type = major
	desc = end_renovation_bulwari_temple_decision_desc
	selection_tooltip = end_renovation_bulwari_temple_decision_tooltip

	is_shown = {
		title:b_eduz_rakadags.title_province = {
			has_province_modifier = renovating_bulwar_temple
		}
	}

	is_valid = {
	}

	effect = {
		title:b_eduz_rakadags.title_province = {
			remove_building = temple_bulwar_02
			add_building = temple_bulwar_01
			remove_province_modifier = renovating_bulwar_temple
		}
	}

	hidden_effect = {
		gold = 600
	}
}