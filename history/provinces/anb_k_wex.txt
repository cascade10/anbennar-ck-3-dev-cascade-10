﻿#k_wex
##d_wexhills
###c_wexkeep
306 = {		#Wexkeep

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_hyl_01
		special_building = castanorian_citadel_bal_hyl_01
	}
}
2132 = {

    # Misc
    holding = church_holding

    # History

}

###c_ironhill
303 = {		#Ironhill

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = wexhills_mines_01
	}
}
2133 = {

    # Misc
    holding = city_holding

    # History

}
2134 = {

    # Misc
    holding = none

    # History

}

###c_wexhills
304 = {		#Wexhills

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2129 = {

    # Misc
    holding = church_holding

    # History

}
2130 = {

    # Misc
    holding = city_holding

    # History

}
2131 = {

    # Misc
    holding = none

    # History

}

###c_threeguard
305 = {		#Threeguard

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2126 = {

    # Misc
    holding = city_holding

    # History

}
###b_amberflow
2127 = {

    # Misc
    holding = castle_holding

    # History

}
2128 = {

    # Misc
    holding = none

    # History

}

##d_escandar
###c_escandar
316 = {		#Escandar

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = calasandur_castle_escandar_01
		special_building = calasandur_castle_escandar_01
	}
}
2108 = {

    # Misc
    holding = city_holding

    # History

}
2109 = {

    # Misc
    holding = none

    # History

}

###c_gnollsgate
913 = {		#Gnollsgate

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2106 = {

    # Misc
    holding = none

    # History

}
2107 = {

    # Misc
    holding = church_holding

    # History

}

###c_vaduham
317 = {		#Rupellion

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2110 = {

    # Misc
	holding = none

    # History

}

##d_bisan
###c_bisan
300 = {		#Bisan

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2094 = {

    # Misc
    holding = city_holding

    # History

}
2095 = {

    # Misc
    holding = church_holding

    # History

}
2096 = {

    # Misc
    holding = none

    # History

}

###c_countsbridge
315 = {		#Countsbridge

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2097 = {

    # Misc
    holding = city_holding

    # History

}

2098 = {

    # Misc
    holding = none

    # History

}

###c_hiltsford
579 = {		#Hiltsford

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2093 = {

    # Misc
    holding = city_holding

    # History

}

###c_corseton
2092 = {

    # Misc
    holding = church_holding
	special_building = holy_site_the_necropolis_01

    # History

}

332 = {

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2091 = {

    # Misc
    holding = none

    # History

}

###c_magdalaire
2089 = {	#Arca Magda

    # Misc
    culture = wexonard
    religion = border_cults
    holding = castle_holding

    # History
}
294 = {		#Magdalaire

    # Misc
    holding = city_holding

    # History
}
923 = {		#Aelthil

    # Misc
    holding = castle_holding

    # History
}
2088 = {

    # Misc
    holding = none

    # History

}
2090 = {

    # Misc
    holding = none

    # History

}

##d_ottocam
###c_ottocam
307 = {		#Ottocam

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2121 = {

    # Misc
    holding = church_holding

    # History

}

2122 = {

    # Misc
    holding = none

    # History

}

###c_indlebury
903 = {		#Indlebury

    # Misc
    culture = wexonard
    religion = border_cults

    # History
}
2118 = {

    # Misc
    holding = none

    # History

}
2119 = {

    # Misc
    holding = city_holding

    # History

}
2120 = {

    # Misc
    holding = church_holding

    # History

}

###c_vinerick
907 = {		#Vinerick

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2123 = {

    # Misc
    holding = city_holding

    # History

}

2124 = {

    # Misc
    holding = church_holding

    # History

}

2125 = {

    # Misc
    holding = none

    # History

}

##d_greater_bardswood
###c_bardswood
918 = {		#Bardswood

    # Misc
    culture = milcori
    religion = milcori_cult
	holding = tribal_holding

    # History
}
2103 = {

    # Misc
    holding = city_holding

    # History

}
2104 = {

    # Misc
    holding = none

    # History

}
2105 = {

    # Misc
    holding = none

    # History

}

###c_stumpwood
318 = {		#Stumpwood

    # Misc
    culture = milcori
    religion = milcori_cult
	holding = castle_holding

    # History
}
2101 = {

    # Misc
    holding = none

    # History

}
2102 = {

    # Misc
    holding = church_holding

    # History

}

###c_butchersfield
319 = {		#Butchersfield

    # Misc
    culture = milcori
    religion = border_cults
	holding = castle_holding

    # History
}
2099 = {

    # Misc
    holding = city_holding

    # History

}
2100 = {

    # Misc
    holding = none

    # History

}

##d_sugamber
###c_rhinmond
919 = {		#Rhinmond

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2111 = {

    # Misc
    holding = church_holding

    # History

}
2112 = {

    # Misc
    holding = none

    # History

}
2113 = {

    # Misc
    holding = city_holding

    # History

}
2114 = {

    # Misc
    holding = none

    # History

}

###c_lockcastle
415 = {		#Lockcastle

    # Misc
    culture = wexonard
    religion = border_cults
	holding = castle_holding

    # History
}
2115 = {

    # Misc
    holding = none

    # History

}
2116 = {

    # Misc
    holding = city_holding

    # History

}
2117 = {

    # Misc
    holding = none

    # History

}
