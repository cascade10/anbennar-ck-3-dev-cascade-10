﻿k_marrhold = {
	1000.1.1 = { change_development_level = 8 }
	989.7.18 = {
		holder = 91 #Hoger Marr
	}
}

d_marrhold = {
	1000.1.1 = { change_development_level = 10 }
}

c_marrhold = {
	1000.1.1 = { change_development_level = 12 }
}

c_marrhyl = {
	1000.1.1 = { change_development_level = 8 }
}

c_willowmore = {
	1000.1.1 = { change_development_level = 7 }
}

d_dryadsdale= {
	1013.08.3 = {
		holder = escanni_0020
	}
	
	1020.08.3 = {
		liege = "k_marrhold"
	}
}
c_pixiebury = {
	1000.1.1 = { change_development_level = 9 }
	1020.08.3 = {
		holder = escanni_0020
	}
	
	1020.08.3 = {
		liege = "d_dryadsdale"
	}
}

c_spritescage = {
	1020.08.3 = {
		holder = escanni_0020
	}
	
	1004.08.3 = {
		liege = "d_dryadsdale"
	}
}

c_dryadsdale = {
	1000.1.1 = { change_development_level = 7 }
		1013.08.3 = {
		holder = escanni_0022
	}
	
	1004.08.3 = {
		liege = "d_dryadsdale"
	}
}

c_griffonsgate = {
	
	1000.4.2 = {
		holder = escanni_0018
	}
	1013.08.3 = {
		holder = escanni_0019
	}
	
	1004.2.3 = {
		liege = "k_marrhold"
	}
}

d_hornwood = { 
    1003.08.3 = {
		holder = escanni_0027
	}
	
	1020.08.3 = {
		liege = "k_marrhold"
	}
}
c_nymphlea = {
		1003.08.3 = {
		holder = escanni_0027
	}
	1004.08.3 = {
		liege = "d_hornwood"
	}
}

c_lovers_quarrel = { 
		1003.08.3 = {
		holder = escanni_0027
	}
	
	1004.08.3 = {
		liege = "d_hornwood"
	}
}

c_hornwood = { 
		1003.08.3 = {
		holder = hornwood_0001
	}
	
	1004.08.3 = {
		liege = "d_hornwood"
	}
}


c_willowmore = {
		996.08.3 = {
		holder = escanni_0030
	}
	
	1004.08.3 = {
		liege = "k_marrhold"
	}
}

c_doescker = {
   998.08.3 = {
		holder = escanni_0033
	}
	
   1012.08.3 = {
		holder = escanni_0032
	}
	
	1004.08.3 = {
		liege = "k_marrhold"
	}
}